package macela;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import org.slf4j.LoggerFactory;


public class Main {

  private static final String unformattedFilesPath = "src/main/resources/DB/Unformatted/CF documents/";
  private static String formattedFilesPath = "src/main/resources/DB/Formatted/CF documents/";

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Main.class);
  private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
  private static String unformattedQueryFile = "src/main/resources/DB/Unformatted/CF queries/cfquery";
  private static String formattedQueriesPath = "src/main/resources/DB/Formatted/CF queries/";


  public static ArrayList<Document> readDocumentsFile(final File file) {
    final ArrayList<Document> docs = new ArrayList();
    try {
      String line;
      String field = "";
      int start;
      final BufferedReader reader = new BufferedReader(new FileReader(file));
      Document doc = new Document();
      while ((line = reader.readLine()) != null) {
        if (line.length() < 2) {
          continue;
        }
        if (!line.substring(0, 2).contains(" ")) {
          field = line.substring(0, 2);
          start = 3;
        } else {
          start = 2;
        }
        if (field.contains("PN")) {
          if (!doc.getPaperNumber().isEmpty()) {
            docs.add(doc);
          }
          doc = new Document();
          doc.setPaperNumber(doc.getPaperNumber().concat(line.substring(start)));
        }
        if (field.contains("RN")) {
          doc.setRecordNumber(doc.getRecordNumber().concat(line.substring(start)));
        }
        if (field.contains("AN")) {
          doc.setAcessionNumber(doc.getAcessionNumber().concat(line.substring(start)));
        }
        if (field.contains("AU")) {
          doc.addAuthor(line.substring(start).trim());
        }
        if (field.contains("TI")) {
          doc.setTitle(doc.getTitle().concat(line.substring(start)));
        }
        if (field.contains("SO")) {
          doc.setSource(doc.getSource().concat(line.substring(start)));
        }
        if (field.contains("MJ")) {
          doc.addMajorSubject(line.substring(start));
        }
        if (field.contains("MN")) {
          doc.addMinorSubject(line.substring(start));
        }
        if (field.contains("AB") || field.contains("EX")) {
          doc.setAbstractExtract(doc.getAbstractExtract().concat(line.substring(start)));
        }
        if (field.contains("RF")) {
          doc.addReference(line.substring(start));
        }
        if (field.contains("CT")) {
          doc.addCitation(line.substring(start));
        }

      }
      docs.add(doc);

    } catch (final IOException ex) {
      logger.error("IOException while parsing file.", ex);
    }
    return docs;
  }


  public static void main(final String[] args) {

    final File unformattedDocuments = new File(unformattedFilesPath);
    final File unformattedQuery = new File(unformattedQueryFile);
    parseCFDocuments(unformattedDocuments);
    parseCFQueryFile(unformattedQuery);


  }

  private static ArrayList<Query> parseCFQueryFile(final File unformattedQuery) {
    logger.info("Parsing queries from file: {}", unformattedQuery.getAbsoluteFile());
    final ArrayList<Query> queries = new ArrayList();
    try {
      String line;
      String field = "";
      int start;
      final BufferedReader reader = new BufferedReader(new FileReader(unformattedQuery));
      Query query = new Query();
      while ((line = reader.readLine()) != null) {
        if (line.length() < 2) {
          continue;
        }
        if (!line.substring(0, 2).contains(" ")) {
          field = line.substring(0, 2);
          start = 3;
        } else {
          start = 2;
        }
        if (field.contains("QN")) {
          if (!query.getQueryNumber().isEmpty()) {
            queries.add(query);
          }
          query = new Query();
          query.setQueryNumber(query.getQueryNumber().concat(line.substring(start)));
        }
        if (field.contains("QU")) {
          query.setQueryContent(query.getQueryContent().concat(line.substring(start)));
        }
        if (field.contains("NR")) {
          query.setNumberOfRelevantDocuments(line.substring(start));
        }
        if (field.contains("RD")) {
          query.addRelevantDoc(line.substring(start));
        }


      }
      queries.add(query);

    } catch (final IOException ex) {
      logger.error("IOException while parsing file.", ex);
    }
    logger.info("Parsed a total of : {} queries.", queries.size());

    for (int i = 0; i < queries.size(); i++) {
      try (Writer writer = new FileWriter(
          formattedQueriesPath + queries.get(i).getQueryNumber() + ".json")) {
        gson.toJson(queries.get(i).prepareForSerialization(), writer);
      } catch (final Exception e) {
        e.printStackTrace();
      }
    }
    logger.info("Parsed queries can be found at: {}", formattedQueriesPath);

    return queries;
  }


  /**
   * Parse Cystic Fibrosis docs
   */
  private static void parseCFDocuments(final File folder) {
    logger.debug("Reading files from directory: {}", folder.getAbsolutePath());

    final String[] files = folder.list((dir, name) -> {
      if (name.contains("cf") && !name.contains("query")) {
        return true;
      }
      return false;
    });
    for (final String fileName : files) {
      parseFile(unformattedFilesPath + fileName);
    }
    final File json = new File(formattedFilesPath);
    logger.debug("Parsed: {} files, got {} documents.", files.length, json.listFiles().length);
  }

  /**
   * Given a file, parse it to something that makes sense and save it in JSON
   */
  private static void parseFile(final String filePath) {
    final File f = new File(filePath);
    logger.debug(f.getAbsolutePath());
    final ArrayList<Document> cfcDocuments = readDocumentsFile(f);

    for (int i = 0; i < cfcDocuments.size(); i++) {
      try (Writer writer = new FileWriter(
          formattedFilesPath + cfcDocuments.get(i).getPaperNumber() + ".json")) {
        gson.toJson(cfcDocuments.get(i).prepareForSerialization(), writer);
      } catch (final Exception e) {
        e.printStackTrace();
      }

    }

  }


}
