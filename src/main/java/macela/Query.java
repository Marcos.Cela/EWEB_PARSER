package macela;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Marcos Cela
 */
public class Query {

  private static final transient Pattern p = Pattern.compile("(\\d+)\\s+(\\d{4})");
  private transient ArrayList<String[]> unformattedResults = new ArrayList<>();

  private String queryNumber = "";
  private String queryText = "";
  private String relevantDocuments = "";
  private String[][] results;

  String getQueryNumber() {
    return queryNumber;
  }

  void setQueryNumber(final String queryNumber) {
    this.queryNumber = String.valueOf(Integer.parseInt(queryNumber.trim()));
  }

  void setQueryContent(final String content) {
    queryText = content.trim();
  }

  String getQueryContent() {
    return queryText;
  }

  void setNumberOfRelevantDocuments(final String n) {
    relevantDocuments = String.valueOf(Integer.parseInt(n.trim()));
  }

  Query prepareForSerialization() {
    results = unformattedResults.toArray(new String[][]{});
    return this;
  }

  void addRelevantDoc(final String line) {
    // "23 2220   47 2221   50 0001   60 0001  114 0011  132 0001  135 0001"
    final Matcher m = p.matcher(line);

    while (m.find()) {
      final String relevantDocumentID = m.group(1);
      final String relevantDocumentScore = m.group(2);
      final String[] pair = {relevantDocumentID, relevantDocumentScore};
      unformattedResults.add(pair);
    }
  }
}
