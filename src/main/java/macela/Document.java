package macela;


import java.util.ArrayList;
import java.util.Arrays;

public class Document {

  private String paperNumber = "";
  private String recordNumber = "";
  private String acessionNumber = "";
  private transient ArrayList<String> authorsNoFormat = new ArrayList<>();
  private String[] authors = {};
  private String title = "";
  private String source = "";
  private transient ArrayList<String> majorSubjectsNoFormat = new ArrayList<>();
  private String[] majorSubjects;
  private transient ArrayList<String> minorSubjectsNoFormat = new ArrayList<>();
  private String[] minorSubjects = {};
  private String abstractExtract = "";
  private transient ArrayList<String> referencesNoFormat = new ArrayList<>();
  private String[] references = {};
  private transient ArrayList<String> citationsNoFormat = new ArrayList<>();
  private String[] citations = {};


  /**
   * Gives format to the fields that don't have format
   */
  Document prepareForSerialization() {
    majorSubjects = majorSubjectsNoFormat.toArray(new String[]{});
    minorSubjects = minorSubjectsNoFormat.toArray(new String[]{});
    references = referencesNoFormat.toArray(new String[]{});
    citations = citationsNoFormat.toArray(new String[]{});
    authors = authorsNoFormat.toArray(new String[]{});
    return this;
  }


  String getPaperNumber() {
    return paperNumber;
  }

  void setPaperNumber(final String paperNumber) {
    this.paperNumber = paperNumber.trim();
  }

  String getRecordNumber() {
    return recordNumber;
  }

  void setRecordNumber(final String recordNumber) {
    this.recordNumber = recordNumber.trim();
  }


  String getTitle() {
    return title;
  }

  void setTitle(final String title) {
    this.title = title.trim();
  }

  String getSource() {
    return source;
  }

  void setSource(final String source) {
    this.source = source.trim();
  }


  String getAbstractExtract() {
    return abstractExtract;
  }

  void setAbstractExtract(final String abstractExtract) {
    this.abstractExtract = abstractExtract.trim();
  }


  String getAcessionNumber() {
    return acessionNumber;
  }

  void setAcessionNumber(final String acessionNumber) {
    this.acessionNumber = acessionNumber.trim();
  }

  public String toString() {
    return this.acessionNumber;
  }

  void addCitation(final String citation) {
    citationsNoFormat.add(citation.trim());

  }

  void addMajorSubject(final String majorSub) {
    final String[] res = majorSub.split("\\. ");
    for (int i = 0; i < res.length; i++) {
      res[i] = res[i].trim();
    }
    majorSubjectsNoFormat.addAll(Arrays.asList(res));
  }

  void addMinorSubject(final String minorSub) {
    final String[] res = minorSub.split("\\. ");
    for (int i = 0; i < res.length; i++) {
      res[i] = res[i].trim();
    }
    minorSubjectsNoFormat.addAll(Arrays.asList(res));
  }

  void addReference(final String reference) {
    referencesNoFormat.add(reference.trim());
  }

  void addAuthor(final String auth) {
    final String[] res = auth.split("\\. ");
    for (int i = 0; i < res.length; i++) {
      res[i] = res[i].trim();
    }
    authorsNoFormat.addAll(Arrays.asList(res));
  }
}
